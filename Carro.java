public class Carro {
    
    int qtdrodas;
    int qtdportas;
    String cores;
    String arcondicionado;
    String somautomotivo;
  
public Carro(int qtdrodas, int qtdportas, String cores, String arcondicionado, String somautomotivo) {
    }

public void setqtdrodas(int qtdrodas) {
    this.qtdrodas = qtdrodas;
}

public void setqtdportas(int qtdportas) {
    this.qtdportas = qtdportas;
}

public void setcores(String cores){
    this.cores = cores;
}

public void setarcondicionado(String arcondicionado){
    this.arcondicionado = arcondicionado;
}

public void setsomautomotivo(String somautomotivo){
    this.somautomotivo = somautomotivo;
}

public int getqtdrodas(){
    return qtdrodas;
}

public int getqtdportas(){
    return qtdportas;
}

public String getcores(){
    return cores;
}

public String getarcondicionado(){
    return arcondicionado;
}

public String getsomautomotivo(){
    return somautomotivo;
}

}

