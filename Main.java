import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Qual a quantidade de rodas deseja para seu veículo?: ");
               int qtdrodas = scanner.nextInt();

               System.out.println("Qual a quantidade de portas para seu veículo?: ");
               int qtdportas = scanner.nextInt();

               System.out.println("Qual cor deseja para seu veículo?: ");
               String cores = scanner.nextLine();

               System.out.println("Deseja ar-condicionado no veículo?: ");
               String arcondicionado = scanner.nextLine();

               System.out.println("Deseja som automotivo no veículo?: ");
               String somautomotivo = scanner.nextLine();

               Carro caracteristicas = new Carro(4, 4, "Vermelho", "Sim", "Sim");
               caracteristicas.setqtdrodas(qtdrodas);
               caracteristicas.setqtdportas(qtdportas);
               caracteristicas.setcores(cores);
               caracteristicas.setarcondicionado(arcondicionado);
               caracteristicas.setsomautomotivo(somautomotivo);
               
               System.out.println("Carro solicitado com sucesso ! ");
               
               System.out.println("Quantidade de Rodas: "+ caracteristicas.getqtdrodas());
               System.out.println("Quantidade de Portas: " + caracteristicas.getqtdportas());
               System.out.println("Cor do Veículo: " + caracteristicas.getcores());
               System.out.println("Carro com ar Condicionado ?: " + caracteristicas.getarcondicionado());
               System.out.println("Carro com Som Automotivo ?: " + caracteristicas.getsomautomotivo());
        }
    
                    
        }

    }